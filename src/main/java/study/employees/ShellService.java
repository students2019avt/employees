package study.employees;

import java.io.*;
import java.util.*;

public class ShellService {

    private PrintStream outputStream = System.out;

    private boolean terminate;
    private Map<String, Command> commands = new LinkedHashMap<>();

    public void setCommands(List<Command> commandsList) {
        for (Command command : commandsList) {
            addCommand(command);
        }
    }

    private class ExitCommand implements Command {

        public String getName() {
            return "exit";
        }

        public String getDescription() {
            return "Exit application.";
        }

        public void execute(String[] args) {
            terminate = true;
            outputStream.println("Good bye!");
        }
    }

    private class HelpCommand implements Command {

        public String getName() {
            return "help";
        }

        public String getDescription() {
            return "Display help information.";
        }

        public void execute(String[] args) {
            for (Command command : commands.values()) {
                outputStream.println(command.getName() + " - " + command.getDescription());
            }
        }
    }

    public ShellService() {
        addCommand(new SomeCommand());
        addCommand(new HelpCommand());
        addCommand(new ExitCommand());
    }

    public void addCommand(Command command) {
        commands.put(command.getName(), command);
    }

    public void run() throws IOException {
        outputStream.println();
        outputStream.println("You can use 'help':");

        while (!terminate) {
            outputStream.print("> ");

            Scanner scanner = new Scanner(System.in);
            String line = scanner.nextLine();
            String[] args = line.split(" ");
            if (args.length > 0) {
                if (commands.containsKey(args[0])) {
                    Command command = commands.get(args[0]);
                    command.execute(args);
                } else {
                    outputStream.println("No such command!");
                }
            }
        }
    }
}
