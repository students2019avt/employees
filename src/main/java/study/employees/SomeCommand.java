package study.employees;

public class SomeCommand implements Command {

    @Override
    public String getName() {
        return "some";
    }

    @Override
    public String getDescription() {
        return "some command";
    }

    @Override
    public void execute(String[] args) {
        for (int i = 1; i < args.length; i++) {
            System.out.println(args[i]);
        }
    }
}
